# he - print brief help about a single option or command
by Mikel Ward <mikel@mikelward.com>

```bash
# Example Usage:
$he bash continue
$he rsync -v
```
Cloned from: 
https://raw.githubusercontent.com/mikelward/scripts/main/he

